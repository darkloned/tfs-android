package com.fintech.darkloned.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

public class DocumentFragment extends Fragment
{
    int id;

    static DocumentFragment newInstance(int id)
    {
        DocumentFragment document = new DocumentFragment();
        Bundle arguments = new Bundle();

        arguments.putInt("ID", id);
        document.setArguments(arguments);

        return document;
    }

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        Bundle arguments = getArguments();

        if (arguments != null)
        {
            id = arguments.getInt("ID");
        }
    }

    @Override
    public View onCreateView(@NonNull LayoutInflater layoutInflater,
                             ViewGroup container,
                             Bundle savedInstanceState)
    {
        return layoutInflater.inflate(R.layout.fragment_document, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        TextView documentLabel = view.findViewById(R.id.document_label);

        // String concatenation directly in setText() is objectionable
        documentLabel.setText(String.valueOf("Document №" + id));
    }
}
