package com.fintech.darkloned.fragments;

import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fabAddDocument = findViewById(R.id.fab_add_document);

        fabAddDocument.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                int backStackSize = getSupportFragmentManager().getBackStackEntryCount();

                getSupportFragmentManager()
                        .beginTransaction()
                        .replace(R.id.document_holder, DocumentFragment.newInstance(backStackSize + 1))
                        .addToBackStack(null)
                        .commit();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        if (item.getItemId() == R.id.menu_delete_document)
        {
            getSupportFragmentManager().popBackStack();
        }

        return super.onOptionsItemSelected(item);
    }
}
