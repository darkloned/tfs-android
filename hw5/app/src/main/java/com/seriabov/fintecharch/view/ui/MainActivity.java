package com.seriabov.fintecharch.view.ui;

import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.seriabov.fintecharch.R;
import com.seriabov.fintecharch.vm.AppViewModel;

public class MainActivity extends AppCompatActivity
{

    private CoinsAdapter coinsAdapter;
    private AppViewModel viewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        viewModel = ViewModelProviders.of(this).get(AppViewModel.class);

        Toolbar toolbar = findViewById(R.id.toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);

        setSupportActionBar(toolbar);
        fab.setOnClickListener(view -> viewModel.onUpdateCoinsRateClicked());

        View loadingView = findViewById(R.id.loading_layout);
        View contentView = findViewById(R.id.main_recycler_view);
        View errorView = findViewById(R.id.error_layout);

        initRecyclerView();

        viewModel.getLoadingStatus().observe(this,
                visible -> loadingView.setVisibility(
                        visible ? View.VISIBLE : View.GONE
                ));
        viewModel.getContentStatus().observe(this,
                visible -> contentView.setVisibility(
                        visible ? View.VISIBLE : View.GONE
                ));
        viewModel.getErrorStatus().observe(this,
                visible -> errorView.setVisibility(
                        visible ? View.VISIBLE : View.GONE
                ));
        viewModel.getCoinsInfo().observe(this,
                coinsInfo -> coinsAdapter.setData(coinsInfo));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        getMenuInflater().inflate(R.menu.menu_main, menu);

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        int id = item.getItemId();

        if (id == R.id.action_refresh)
        {
            viewModel.onUpdateCoinsRateClicked();

            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    private void initRecyclerView()
    {
        RecyclerView recyclerView = findViewById(R.id.main_recycler_view);

        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        coinsAdapter = new CoinsAdapter(coinInfo -> DetailsActivity.start(MainActivity.this, coinInfo));

        recyclerView.setAdapter(coinsAdapter);
    }
}
