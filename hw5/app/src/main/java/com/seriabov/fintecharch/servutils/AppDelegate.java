package com.seriabov.fintecharch.servutils;

import android.app.Application;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import com.seriabov.fintecharch.BuildConfig;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class AppDelegate extends Application
{

    private static AppDelegate instance;
    private Api apiService;

    private AppDelegate()
    {
        OkHttpClient client = new OkHttpClient.Builder()
                .addInterceptor(new HttpLoggingInterceptor().setLevel(HttpLoggingInterceptor.Level.BODY))
                .build();

        apiService = new Retrofit.Builder()
                .baseUrl(BuildConfig.API_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .build()
                .create(Api.class);
    }

    public static AppDelegate getInstance()
    {
        if (instance == null)
        {
            instance = new AppDelegate();
        }

        return instance;
    }

    public Api getApiService()
    {
        return apiService;
    }
}

