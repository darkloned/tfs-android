package com.seriabov.fintecharch;

import android.app.Application;
import android.content.Context;

import timber.log.Timber;

public class App extends Application
{

    public static App from(Context context)
    {
        return (App) context.getApplicationContext();
    }

    @Override
    public void onCreate()
    {
        super.onCreate();

        if (BuildConfig.DEBUG)
        {
            Timber.plant(new Timber.DebugTree());
        }
    }
}
