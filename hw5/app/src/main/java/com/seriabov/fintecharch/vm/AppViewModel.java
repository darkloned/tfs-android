package com.seriabov.fintecharch.vm;

import android.arch.lifecycle.ViewModel;
import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.MutableLiveData;
import android.support.annotation.NonNull;

import com.seriabov.fintecharch.model.CoinInfo;
import com.seriabov.fintecharch.servutils.AppDelegate;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AppViewModel extends ViewModel
{
    private MutableLiveData<Boolean> loadingStatus = new MutableLiveData<>();
    private MutableLiveData<Boolean> contentStatus = new MutableLiveData<>();
    private MutableLiveData<Boolean> errorStatus = new MutableLiveData<>();
    private MutableLiveData<List<CoinInfo>> coinsInfo = new MutableLiveData<>();

    public LiveData<Boolean> getLoadingStatus()
    {
        return loadingStatus;
    }

    public LiveData<Boolean> getContentStatus()
    {
        return contentStatus;
    }

    public LiveData<Boolean> getErrorStatus()
    {
        return errorStatus;
    }

    public LiveData<List<CoinInfo>> getCoinsInfo()
    {
        return coinsInfo;
    }

    public void onUpdateCoinsRateClicked()
    {
        loadingStatus.setValue(true);
        contentStatus.setValue(false);
        errorStatus.setValue(false);

        AppDelegate.getInstance().getApiService()
                .getCoinsList()
                .enqueue(new Callback<List<CoinInfo>>()
                {
                    @Override
                    public void onResponse(@NonNull Call<List<CoinInfo>> call, @NonNull Response<List<CoinInfo>> response)
                    {
                        coinsInfo.setValue(response.body());
                        loadingStatus.setValue(false);
                        contentStatus.setValue(true);
                    }

                    @Override
                    public void onFailure(@NonNull Call<List<CoinInfo>> call, @NonNull Throwable t)
                    {
                        loadingStatus.setValue(false);
                        errorStatus.setValue(true);
                    }
                });
    }
}
