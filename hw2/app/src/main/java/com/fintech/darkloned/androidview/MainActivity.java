package com.fintech.darkloned.androidview;

import android.os.Bundle;
import android.support.design.chip.Chip;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

public class MainActivity extends AppCompatActivity
{
    LinearLayout topViewGroup, bottomViewGroup;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        HashMap<String, String> metroStations = new HashMap<>();

        metroStations.put("Сокольники", "red");
        metroStations.put("Красные Ворота", "red");
        metroStations.put("Лубянка", "red");
        metroStations.put("Фрунзенская", "red");
        metroStations.put("Воробьёвы горы", "red");

        metroStations.put("Водный стадион", "green");
        metroStations.put("Войковская", "green");
        metroStations.put("Сокол", "green");
        metroStations.put("Аэропорт", "green");
        metroStations.put("Динамо", "green");

        metroStations.put("Арбатская", "blue");
        metroStations.put("Смоленская", "blue");
        metroStations.put("Студенческая", "blue");
        metroStations.put("Кутузовская", "blue");
        metroStations.put("Фили", "blue");

        metroStations.put("Перово", "yellow");
        metroStations.put("Авиамоторная", "yellow");
        metroStations.put("Минская", "yellow");
        metroStations.put("Раменки", "yellow");
        metroStations.put("Новопеределкино", "yellow");

        HashMap<String, Integer> colors = new HashMap<>();

        colors.put("red", R.color.red);
        colors.put("green", R.color.green);
        colors.put("blue", R.color.blue);
        colors.put("yellow", R.color.yellow);

        topViewGroup = findViewById(R.id.topViewGroup);
        bottomViewGroup = findViewById(R.id.bottomViewGroup);

        while (!metroStations.isEmpty())
        {
            ArrayList<String> stationNames = new ArrayList<>(metroStations.keySet());
            String randomStationName = stationNames.get(new Random().nextInt(stationNames.size()));

            Chip stationChip = new Chip(this);

            stationChip.setChipText(randomStationName);
            stationChip.setChipBackgroundColorResource(colors.get(
                    metroStations.get(randomStationName)));
            stationChip.setOnClickListener(this::switchChipSide);

            if (metroStations.size() > 10)
            {
                topViewGroup.addView(stationChip);
            }
            else
            {
                bottomViewGroup.addView(stationChip);
            }

            CustomViewGroup.MarginLayoutParams layoutParams =
                    (CustomViewGroup.MarginLayoutParams) stationChip.getLayoutParams();

            layoutParams.setMargins(0, 0, 15, 15);
            stationChip.setLayoutParams(layoutParams);

            metroStations.remove(randomStationName);
        }
    }

    private void switchChipSide(View chip)
    {
        if (chip.getParent().equals(topViewGroup))
        {
            topViewGroup.removeView(chip);
            bottomViewGroup.addView(chip);
        }
        else
        {
            bottomViewGroup.removeView(chip);
            topViewGroup.addView(chip);
        }
    }
}
