package person.favorites

import person.types.PersonWithAddress
import io.reactivex.Observable

internal class FavoritesRepository(private val personBackend: PersonBackend, private val favoritesDatabase: FavoritesDatabase) {


    fun loadFavorites(): Observable<List<PersonWithAddress>> {

        /**
         * Provide an observable that only emits a list of PersonWithAddress if they are marked as favorite ones.
         */

       return personBackend.loadAllPersons()
               .flatMap {
                   allPersons -> favoritesDatabase.favoriteContacts()
                       .flatMap { // filter from all persons only favourite IDs
                           favouriteIDs -> Observable.fromArray(allPersons.filter {
                               favouriteIDs.contains(it.person.id)
                           })
                       }
               }
    }
}
