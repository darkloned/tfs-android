package fibonacci;

import java.util.HashMap;

import io.reactivex.Observable;

public class FibonacciExercises {

	public Observable<Integer> fibonacci(int n) {
		return Observable.range(0, n - 3).scan(new HashMap<String, Integer>(), (m, k) ->
		{
			int first, second;

			try {
				first  = m.get("first");
				second = m.get("second");
			}
			catch (NullPointerException ex) {
				first  = 0;
				second = 1;
			}

			m.put("first", second);
			m.put("second", first + second);

			return m;
		})
				.filter(m -> m.size() == 2)
				.map(m -> m.get("first") + m.get("second"))
                .startWith(Observable.fromArray(0, 1, 1));
	}
}


