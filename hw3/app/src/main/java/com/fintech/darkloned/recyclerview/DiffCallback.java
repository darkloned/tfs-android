package com.fintech.darkloned.recyclerview;

import android.support.v7.util.DiffUtil;

import java.util.List;

public class DiffCallback extends DiffUtil.Callback
{
    private List<Worker> oldList, newList;

    DiffCallback(List<Worker> oldList, List<Worker> newList)
    {
        this.oldList = oldList;
        this.newList = newList;
    }

    @Override
    public int getOldListSize()
    {
        return oldList.size();
    }

    @Override
    public int getNewListSize()
    {
        return newList.size();
    }

    @Override
    public boolean areItemsTheSame(int oldItemPosition, int newItemPosition)
    {
        Worker oldWorker = oldList.get(oldItemPosition);
        Worker newWorker = newList.get(newItemPosition);

        return oldWorker.getId() == newWorker.getId();
    }

    @Override
    public boolean areContentsTheSame(int oldItemPosition, int newItemPosition)
    {
        Worker oldWorker = oldList.get(oldItemPosition);
        Worker newWorker = newList.get(newItemPosition);

        return oldWorker.equals(newWorker);
    }
}
