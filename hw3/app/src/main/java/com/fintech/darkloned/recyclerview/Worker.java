package com.fintech.darkloned.recyclerview;

public class Worker
{
    private int id;
    private Integer photo;
    private String name, age, position;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public Integer getPhoto()
    {
        return photo;
    }

    public void setPhoto(Integer photo)
    {
        this.photo = photo;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }

    public String getAge()
    {
        return age;
    }

    public void setAge(String age)
    {
        this.age = age;
    }

    public String getPosition()
    {
        return position;
    }

    public void setPosition(String position)
    {
        this.position = position;
    }

    public boolean equals(Worker other)
    {
        return  photo.equals(other.photo)   &&
                name.equals(other.name)     &&
                age.equals(other.age)       &&
                position.equals(other.position);
    }
}
