package com.fintech.darkloned.recyclerview;

public interface ItemTouchHelperAdapter
{
    void onItemMove(int fromPosition, int toPosition);
    void onItemDismiss(int position);
}
