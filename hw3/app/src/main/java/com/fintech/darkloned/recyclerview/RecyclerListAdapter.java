package com.fintech.darkloned.recyclerview;

import android.support.annotation.NonNull;
import android.support.v7.util.DiffUtil;
import android.support.v7.util.DiffUtil.DiffResult;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;
import java.util.Collections;

public class RecyclerListAdapter extends RecyclerView.Adapter<RecyclerListAdapter.ListViewHolder>
    implements ItemTouchHelperAdapter
{
    private List<Worker> workersList;

    RecyclerListAdapter(List<Worker> workersList)
    {
        this.workersList = workersList;
    }

    void addWorkers(List<Worker> newWorkers)
    {
        DiffCallback diffCallback = new DiffCallback(workersList, newWorkers);
        DiffResult diffResult = DiffUtil.calculateDiff(diffCallback);

        workersList.addAll(newWorkers);
        diffResult.dispatchUpdatesTo(this);
    }

    @Override
    public int getItemCount()
    {
        return workersList.size();
    }

    @Override
    public void onItemMove(int fromPosition, int toPosition)
    {
        Collections.swap(workersList, fromPosition, toPosition);
        notifyItemMoved(fromPosition, toPosition);
    }

    @Override
    public void onItemDismiss(int position)
    {
        workersList.remove(position);
        notifyItemRemoved(position);
    }

    @NonNull
    @Override
    public ListViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType)
    {
        return new ListViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.worker, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ListViewHolder holder, int position)
    {
        holder.bind(workersList.get(position));
    }

    class ListViewHolder extends RecyclerView.ViewHolder
    {
        ImageView image;
        TextView name, age, position;

        ListViewHolder(View itemView)
        {
            super(itemView);

            image    = itemView.findViewById(R.id.image);
            name     = itemView.findViewById(R.id.name);
            age      = itemView.findViewById(R.id.age);
            position = itemView.findViewById(R.id.position);
        }

        void bind(Worker worker)
        {
            image.setImageResource(worker.getPhoto());
            name.setText(worker.getName());
            age.setText(worker.getAge());
            position.setText(worker.getPosition());
        }
    }
}
