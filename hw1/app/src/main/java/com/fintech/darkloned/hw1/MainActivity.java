package com.fintech.darkloned.hw1;

import android.content.Intent;
import android.content.res.Configuration;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class MainActivity extends AppCompatActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        /*
         * It's okay, I guess?.. to avoid code duplication.
         * If we already have setContentView() and setOnClickListener()
         * over there, why not to use them here?
         */
        onConfigurationChanged(null);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig)
    {
        super.onConfigurationChanged(newConfig);

        /*
         * It automatically chooses between 'layout' and 'layout-land',
         * so no need in using 'newConfig.orientation == Configuration.ORIENTATION_...'
         * (Also Java prohibits getting 'R.layout-land.activity_main' resource, so it's the only way)
         */
        setContentView(R.layout.activity_main);

        findViewById(R.id.startSecondaryActivityButton)
                .setOnClickListener(startSecondaryActivityButtonListener);
    }

    /*
     * We could also do it using [ android:onClick="startSecondaryActivity" ]
     * as a button attribute in activity_main.xml,
     * but OnClickListener() is here for educational purposes, I guess.
     */
    private final View.OnClickListener startSecondaryActivityButtonListener = new View.OnClickListener()
    {
        @Override
        public void onClick(View v)
        {
            Intent intent = new Intent(getApplicationContext(), SecondaryActivity.class);

            startActivity(intent);
        }
    };
}
