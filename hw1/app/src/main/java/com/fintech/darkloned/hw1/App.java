package com.fintech.darkloned.hw1;

import android.app.Activity;
import android.app.Application;
import android.os.Bundle;
import android.util.Log;

public class App extends Application
{
    private static final String TAG = "DEBUG_MSG";

    @Override
    public void onCreate()
    {
        super.onCreate();

        registerActivityLifecycleCallbacks(new ActivityLifecycleCallbacks()
        {
            @Override
            public void onActivityCreated(Activity activity, Bundle savedInstanceState)
            {
                Log.d(TAG, "on" + activity.getClass().getSimpleName() + "Created() triggered");
            }

            @Override
            public void onActivityStarted(Activity activity)
            {
                Log.d(TAG, "on" + activity.getClass().getSimpleName() + "Started() triggered");
            }

            @Override
            public void onActivityResumed(Activity activity)
            {
                Log.d(TAG, "on" + activity.getClass().getSimpleName() + "Resumed() triggered");
            }

            @Override
            public void onActivityPaused(Activity activity)
            {
                Log.d(TAG, "on" + activity.getClass().getSimpleName() + "Paused() triggered");
            }

            @Override
            public void onActivityStopped(Activity activity)
            {
                Log.d(TAG, "on" + activity.getClass().getSimpleName() + "Stopped() triggered");
            }

            @Override
            public void onActivitySaveInstanceState(Activity activity, Bundle outState)
            {
                Log.d(TAG, "on" + activity.getClass().getSimpleName() + "SaveInstanceState() triggered");
            }

            @Override
            public void onActivityDestroyed(Activity activity)
            {
                Log.d(TAG, "on" + activity.getClass().getSimpleName() + "Destroyed() triggered");
            }
        });
    }
}
