package com.fintech.darkloned.hw1;

import android.graphics.drawable.Drawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class SecondaryActivity extends AppCompatActivity
{
    Drawable picture;
    SecondaryActivity previousSecondaryActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_secondary);

        previousSecondaryActivity = (SecondaryActivity) getLastNonConfigurationInstance();

        picture = previousSecondaryActivity == null ?
                ((ImageView) findViewById(R.id.picture)).getDrawable() :
                previousSecondaryActivity.picture;
    }

    @Override
    public Object onRetainCustomNonConfigurationInstance()
    {
        return super.onRetainCustomNonConfigurationInstance();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        // destroy reference to avoid leak
        if (previousSecondaryActivity != null)
            previousSecondaryActivity.picture = null;
    }
}
